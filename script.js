const banned_selectors = [
    ".ytd-statement-banner-renderer",
    "ytd-clarification-renderer",
    "ytd-info-panel-container-renderer"
];

const banned_words = [
    "covid",
    "vaccine",
    "politic",
    "Biden",
    "Trump"
];

const poll_timeout = 1000;

/**
 * This blocks each element which matches a blocked selector
 */
function blockSelectors() {
    // block each banned selector
    for (let i in banned_selectors) {
        let targets = document.querySelectorAll(banned_selectors[i]);
        targets.forEach(function(target) {
            target.style.display = 'none';
        });
    }
}

/**
 * This blocks each element containing a blocked word
 */
function blockWords() {
    // block each banned word
    for (let i in banned_words) {
        let word = banned_words[i].toLowerCase();
        let all = document.querySelectorAll("ytd-rich-item-renderer, ytd-compact-video-renderer, ytd-grid-video-renderer, ytd-video-renderer");
        all.forEach(function(target) {
            if (target.textContent.toLowerCase().includes(word)) {
                target.style.display = 'none';
            }
        });
    }
}

/**
 * This runs each block helper
 */
function runBlocker() {
    blockSelectors();
    blockWords();
}

// run for the first time
runBlocker();

// keep running at an interval
setInterval(() => {
    runBlocker()
}, poll_timeout);
