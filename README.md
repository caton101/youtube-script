# YouTube Script

This is a small snippet of JavaScript to hide all YouTube content that contains
a certain word. It only scans the video boxes on the homepage and "watch next"
sections. It will detect words inside the title or the channel name.

Additionally, it can block entire divs or classes. This is used to remove
YouTube's stupid "statement boxes" and other UI bloat.

This is intended for use with the [Enhancer For YouTube][0] browser extension.

## Installation

1. Install the [browser extension][0]
2. Go to the extension's settings
3. Scroll down to `Custom Script`
4. Copy the contents of `script.js` to the textbox
5. Check the box beside `Automatically execute the script when YouTube is
   loaded in a tab`
6. Click `Save`

## Configuration

**Note**: All configuration happens at the top of the file.

You can change which selectors are banned by modifying the `banned_selectors`
array. Note that it must be a valid CSS selector.

You can change which words are banned by modifying the `banned_words` array. It
can also block substrings if you wish to ban a phrase. All banned words are case
insensitive.

You can change how long to wait before rescanning the page by modifying the
`poll_timeout` variable. A shorter time is useful for fast internet but a longer
time is good for slow internet. The time must be in milliseconds. Note that a
time of 0 is equivalent to an infinite `while` loop.

[0]: https://www.mrfdev.com/enhancer-for-youtube
